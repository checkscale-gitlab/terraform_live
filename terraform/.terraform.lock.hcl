# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/random" {
  version = "3.1.2"
  hashes = [
    "h1:5A5VsY5wNmOZlupUcLnIoziMPn8htSZBXbP3lI7lBEM=",
    "zh:0daceba867b330d3f8e2c5dc895c4291845a78f31955ce1b91ab2c4d1cd1c10b",
    "zh:104050099efd30a630741f788f9576b19998e7a09347decbec3da0b21d64ba2d",
    "zh:173f4ef3fdf0c7e2564a3db0fac560e9f5afdf6afd0b75d6646af6576b122b16",
    "zh:41d50f975e535f968b3f37170fb07937c15b76d85ba947d0ce5e5ff9530eda65",
    "zh:51a5038867e5e60757ed7f513dd6a973068241190d158a81d1b69296efb9cb8d",
    "zh:6432a568e97a5a36cc8aebca5a7e9c879a55d3bc71d0da1ab849ad905f41c0be",
    "zh:6bac6501394b87138a5e17c9f3a41e46ff7833ad0ba2a96197bb7787e95b641c",
    "zh:6c0a7f5faacda644b022e7718e53f5868187435be6d000786d1ca05aa6683a25",
    "zh:74c89de3fa6ef3027efe08f8473c2baeb41b4c6cee250ba7aeb5b64e8c79800d",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:b29eabbf0a5298f0e95a1df214c7cfe06ea9bcf362c63b3ad2f72d85da7d4685",
    "zh:e891458c7a61e5b964e09616f1a4f87d0471feae1ec04cc51776e7dec1a3abce",
  ]
}

provider "registry.terraform.io/hashicorp/vault" {
  version     = "3.4.1"
  constraints = "3.4.1"
  hashes = [
    "h1:dXJBo807u69+Uib2hjoBQ68G2+nGXcNZeq/THVyQQVc=",
    "zh:1eb8370a1846e34e2bcc4d11eece5733735784a8eab447bbed3cfd822101b577",
    "zh:2df3989327cea68b2167514b7ebddc67b09340f00bbf3fa85df03c97adfb9d25",
    "zh:3dd1e317264f574985e856296deef71a76464918bf0566eb0d7f6389ea0586bd",
    "zh:9750861f2822482aa608ea5a52b385bc42b2e1f2511094e6a975412618c4495d",
    "zh:9b940e7f78975d29a4d0a116cf43c0bc1cb03bec4ad8d34887d64e6e60bacb9e",
    "zh:9cb6e7ad2a62529d35dacd20695d49c2f02230cb785d46178cc10f4ec80e5a51",
    "zh:a12718689bbcb37bcbb9132c18bffd354fad8ab5c8cb89cec1a0ee85c65b8cb7",
    "zh:a6e38afacca1af4fab04a9f2dc49b8295eb462db68bdc7451352d0f950f804f8",
    "zh:d6e0e994d51b9e07d5713d4796381f9e129e9de962e79caae2b7055f6f68297e",
    "zh:ea4bbef7a1bb2553db473fa304c93845674167b61e8c9677107a96c8c696da12",
    "zh:f985a8b7f4ef7d1eba9cef7d99997ee9c4a54ffe76dab7fa8b1fdec2a9edca7e",
  ]
}

provider "registry.terraform.io/selectel/selectel" {
  version     = "3.8.0"
  constraints = "3.8.0"
  hashes = [
    "h1:57LerTPqZxvTyWIydvsRy0mJrN6DxoO2T9lLdFBDg2A=",
    "zh:0daed553b2153816b8c401c1e253c812e00d80a2a2b88043dd776c38f19968db",
    "zh:2141a1564f7ceedd64c38de774eb9be87650642477f57dba16cafb98f28a08cb",
    "zh:22e30d100ca273904b53863c0df6d1ac971d2865563b2c2ed91f792b809f39d3",
    "zh:41645ebe8117c2318eae8bf0decb1f6ee2a43d960344d953743bc3f2cf149dbe",
    "zh:640bdcb0e0655fbdc438b31d9dfa854799672e89e8e8bee42ee4883c3e900658",
    "zh:66ea91c601406473900d91e69a7d53d14b8591fd608319dccaac49750702cc5c",
    "zh:68d99756daacf93add90d7de2456867d3b4805ba5b51e4ca94d20a1e042b62df",
    "zh:8a88f4af7183c215ad98749a0fcd4c8db6ca2eb05c1d780186159e3a1bea5749",
    "zh:a7c9b44451529ef1d7ffb882cb349798fc2809edd397c23d452ed4467d48d113",
    "zh:c89bfede0ca958f9a40731fa2550437b974338a972dd95d74fb03ef25f87d1a5",
    "zh:caab086315e48c0e4e24c120658f8a24ae4db242b67a6dfd8bb5627633e79fb6",
    "zh:ed6682b1202ae41c3f0c9e97a67900d4bf0fc356a38f2c40fa79863c54070a5c",
    "zh:f30b0c25a0af51265f4af2fb3c7a7ed70fead2d360df779ea37a4bfa4fb6b689",
    "zh:f771f00ef469c273a4acdbca0dc9aff229c3e2c13e6bb6a3d16ceed23c683abe",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "1.47.0"
  constraints = "1.47.0"
  hashes = [
    "h1:6GKN5WfUZVVJ9cBHwN74RVOzMK2xlZx1qwGo8CUtPPk=",
    "zh:110dfeb02d47af0d1c71d41deac7bb0b23d0d5f9f2ac95f81fee7be1b7131852",
    "zh:164c141ca8d1d1b43b866150797f5e15855d48aaddd50a80ca320e638cdbbd3a",
    "zh:1be4fab5de93f2947c35df6676c67bf2ea410ec71f29e4a57661119c2f262d6a",
    "zh:40dc8f1ffc3521786b38427fe2c0f2ec0b102fed4284c08b2d092ebd1617a603",
    "zh:6335190faaeba5bd5a859df7075c6a820a1f492d1f2258296a4fb3170b4f0643",
    "zh:6460c8c651ba96d434e0db0e09cb02b6188861a8a683b1f9a488d0e119deeb71",
    "zh:6ba87e36384c8c165e0853366252451a20a2c1aeec1a2280eb208db200b5ce33",
    "zh:6ebbb699c8673ebbaa5fc880c7b3a8dcf69d51bf242c72b986cf6b919bac4969",
    "zh:719dc962699d17d03f017d27809c68162a2a849e478e04b858cd798be737a26a",
    "zh:7d865928a695dc3558f671da12dbb134d822968d2f0cb4b600ce79c41a0d1d4b",
    "zh:b5c50a6ab05cb18780f4d4b4d48f502d60f4121810127105f5f7d34d2dda9569",
    "zh:df3adcaacfdd3696a9df768beb3ec997410724c5dbd1cc5fd023e6644686bd74",
    "zh:f836c285e5e36ddaa3d0d13f4e89fcb9047356cfb09c77651cdf97b6394af7fa",
    "zh:f91081a4f26b69b8325149353d3199838466e971853a2d1e9c2917051a6fdd6d",
  ]
}
