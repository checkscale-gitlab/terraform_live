data "vault_generic_secret" "vpn" {
	path = "kv/vpn"
}

resource "openstack_compute_keypair_v2" "ssh_key_vpn" {
	name = "ssh_key_vpn"
	region = data.vault_generic_secret.vpn.data["region"]
	public_key = data.vault_generic_secret.vpn.data["ssh_pub_key"]
}

data "openstack_networking_network_v2" "external_net" {
	name = "external-network"
}

resource "openstack_networking_router_v2" "router_vpn" {
	name = "router_vpn"
	external_network_id = data.openstack_networking_network_v2.external_net.id
}

resource "openstack_networking_network_v2" "network_vpn" {
	name = "network_vpn"
}

resource "openstack_networking_subnet_v2" "subnet_vpn" {
	network_id = openstack_networking_network_v2.network_vpn.id
	name = "subnet_vpn"
	cidr       = data.vault_generic_secret.vpn.data["subnet_cidr"]
}

resource "openstack_networking_router_interface_v2" "router_interface_vpn" {
	router_id = openstack_networking_router_v2.router_vpn.id
	subnet_id = openstack_networking_subnet_v2.subnet_vpn.id
}

data "openstack_images_image_v2" "ubuntu_image" {
	most_recent = true
	visibility = "public"
	name = "Ubuntu 20.04 LTS 64-bit"
}

resource "random_string" "random_name_server" {
	length = 16
	special = false
}

resource "openstack_compute_flavor_v2" "flavor_server_vpn" {
	name = "server-vpn-${random_string.random_name_server.result}"
	ram = "2048"
	vcpus = "1"
	disk =  "0"
	is_public = "false"
}

resource "openstack_blockstorage_volume_v3" "volume_server_vpn" {
	name = "volume-for-server-vpn"
	size = "5"
	image_id             = data.openstack_images_image_v2.ubuntu_image.id
	volume_type = data.vault_generic_secret.vpn.data["volume_type"]
	availability_zone = data.vault_generic_secret.vpn.data["zone"]
	enable_online_resize = true
	lifecycle {
		ignore_changes = [image_id]
	}
}

resource "openstack_compute_instance_v2" "server_vpn" {
	name              = "server_vpn"
	flavor_id         = openstack_compute_flavor_v2.flavor_server_vpn.id
  key_pair          = openstack_compute_keypair_v2.ssh_key_vpn.id
  availability_zone = data.vault_generic_secret.vpn.data["zone"]
  network {
    uuid = openstack_networking_network_v2.network_vpn.id
  }
  block_device {
    uuid             = openstack_blockstorage_volume_v3.volume_server_vpn.id
    source_type      = "volume"
    destination_type = "volume"
    boot_index       = 0
  }
  vendor_options {
    ignore_resize_confirmation = true
  }
  lifecycle {
    ignore_changes = [image_id]
  }
}

resource "openstack_networking_floatingip_v2" "fip_tf" {
  pool = "external-network"
}

resource "openstack_compute_floatingip_associate_v2" "fip_tf" {
  floating_ip = openstack_networking_floatingip_v2.fip_tf.address
  instance_id = openstack_compute_instance_v2.server_vpn.id
}

data "selectel_domains_domain_v1" "opusdv" {
	name = "opusdv.online"
}

resource "selectel_domains_record_v1" "a_record_vpn" {
	domain_id = data.selectel_domains_domain_v1.opusdv.id
	name = "${var.subdomain}.${var.domain}"
	type = "A"
	content = openstack_networking_floatingip_v2.fip_tf.address
	ttl = 60
}
